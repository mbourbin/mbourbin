var adjustLayout = function() {
    if ($(window).width() > 1024) $('html').addClass('layout1024');
    else $('html').removeClass('layout1024');
};

$(function() {
    adjustLayout();
});

$(window).on('resize', function() {
    adjustLayout();
});